Name:           colord
Version:        1.4.7
Release:        1
Summary:        A system activated daemon
License:        GPLv2+ and LGPLv2+
URL:            https://www.freedesktop.org/software/colord/
Source0:        https://www.freedesktop.org/software/colord/releases/%{name}-%{version}.tar.xz

BuildRequires:  color-filesystem dbus-devel docbook5-style-xsl gettext glib2-devel
BuildRequires:  gobject-introspection-devel gtk-doc libgudev1-devel sane-backends-devel
BuildRequires:  libxslt meson sqlite-devel  systemd systemd-devel vala bash-completion
BuildRequires:  lcms2-devel >= 2.6 libgusb-devel >= 0.2.2 polkit-devel >= 0.103

Requires:       color-filesystem %{name}-libs = %{version}-%{release}
%{?systemd_requires}
Requires(pre):  shadow-utils
Obsoletes:      colord < 0.1.27-3
Obsoletes:      shared-color-profiles <= 0.1.6-2
Provides:       shared-color-profiles

%description
colord is a system service that makes it easy to manage, install and
generate color profiles to accurately color manage input and output devices.

%package     libs
Summary:     Libraries for %{name}

%description libs
colord is a low level system activated daem that maps color devices
to color profiles in the system context.

%package        devel
Summary:        Development package for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-libs = %{version}-%{release}
Obsoletes:      colorhug-client-devel <= 0.1.13 shared-color-profiles-extra <= 0.1.6-2
Obsoletes:      colord-extra-profiles < %{version}-%{release}
Provides:       colord-extra-profiles = %{version}-%{release} shared-color-profiles-extra
Obsoletes:      colord-tests < %{version}-%{release}
Provides:       colord-tests = %{version}-%{release}

%description    devel
Development package for colord.

%package        help
Summary:        Help documentation package for color
Requires:       %{name} = %{version}-%{release}
Provides:       colord-devel-docs = %{version}-%{release}
Obsoletes:      colord-devel-docs < %{version}-%{release}

%description    help
This help package contains help documents for color.

%prep
%autosetup -p1

%build
ulimit -Sv 2000000

# colord-test-private test cases always fail, so we don't run this case
# https://github.com/hughsie/colord/issues/139 use this link to track bugs
sed -i 's/test('"'"'colord-test-private'"'"'/# test('"'"'colord-test-private'"'"'/g' %{_builddir}/%{name}-%{version}/lib/colord/meson.build

%meson -Dargyllcms_sensor=false -Dbash_completion=false -Ddaemon_user=colord \
       -Dprint_profiles=false -Dvapi=true -Dinstalled_tests=true -Dtests=true
%meson_build

%install
%meson_install
cat >> $RPM_BUILD_ROOT/var/lib/colord/storage.db << EXIT
EXIT
cat >> $RPM_BUILD_ROOT/var/lib/colord/storage.db << EXIT
EXIT
%{_rpmconfigdir}/find-lang.sh %{buildroot} %{name}

%pre
cat /etc/group | grep colord > /dev/null || groupadd -r colord
cat /etc/passwd | grep colord >/dev/null || useradd -r -g colord \
    -s /sbin/nologin -d /var/lib/colord -c "User for colord" colord
exit 0

%check
%meson_test

%preun
%systemd_preun colord.service

%post
%systemd_post colord.service

%postun
%systemd_postun colord.service

%ldconfig_scriptlets libs

%files -f %{name}.lang
%doc AUTHORS
%license COPYING
%{_bindir}/*
%attr(755,colord,colord) %dir %{_localstatedir}/lib/colord/icc
%attr(755,colord,colord) %dir %{_localstatedir}/lib/colord
%{_datadir}/dbus-1/system.d/org.freedesktop.ColorManager.conf
%{_datadir}/dbus-1/system-services/org.freedesktop.ColorManager.service
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorManager*.xml
%{_datadir}/polkit-1/actions/org.freedesktop.color.policy
%{_datadir}/colord
%{_libdir}/colord-plugins
%{_libdir}/colord-sensors
%{_libexecdir}/colord-session
%{_libexecdir}/colord
%{_unitdir}/colord.service
%{_userunitdir}/colord-session.service
%dir %{_icccolordir}/colord
%{_icccolordir}/colord/AdobeRGB1998.icc
%{_icccolordir}/colord/Bluish.icc
%{_icccolordir}/colord/Rec709.icc
%{_icccolordir}/colord/sRGB.icc
%{_icccolordir}/colord/SMPTE-C-RGB.icc
%{_icccolordir}/colord/ProPhotoRGB.icc
%{_icccolordir}/colord/x11-colors.icc
%ghost %attr(-,colord,colord) %{_localstatedir}/lib/colord/*.db
/usr/lib/udev/rules.d/*.rules
/usr/lib/tmpfiles.d/colord.conf
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorHelper.xml
%{_datadir}/dbus-1/services/org.freedesktop.ColorHelper.service
%{_datadir}/glib-2.0/schemas/org.freedesktop.ColorHelper.gschema.xml

%files libs
%doc COPYING
%{_libdir}/libcolord.so.2*
%{_libdir}/libcolordprivate.so.2*
%{_libdir}/libcolorhug.so.2*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%dir %{_libexecdir}/installed-tests/colord
%{_includedir}/colord-1
%{_libdir}/libcolord.so
%{_libdir}/libcolorhug.so
%{_libdir}/libcolordprivate.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/vala/vapi/colord.vapi
%{_datadir}/gir-1.0/*.gir
%{_datadir}/vala/vapi/colord.deps
%{_datadir}/installed-tests/colord/*
%{_libexecdir}/installed-tests/colord/*
%{_icccolordir}/colord/AppleRGB.icc
%{_icccolordir}/colord/BestRGB.icc
%{_icccolordir}/colord/BetaRGB.icc
%{_icccolordir}/colord/BruceRGB.icc
%{_icccolordir}/colord/CIE-RGB.icc
%{_icccolordir}/colord/Crayons.icc
%{_icccolordir}/colord/ColorMatchRGB.icc
%{_icccolordir}/colord/DonRGB4.icc
%{_icccolordir}/colord/ECI-RGBv1.icc
%{_icccolordir}/colord/ECI-RGBv2.icc
%{_icccolordir}/colord/EktaSpacePS5.icc
%{_icccolordir}/colord/Gamma*.icc
%{_icccolordir}/colord/NTSC-RGB.icc
%{_icccolordir}/colord/PAL-RGB.icc
%{_icccolordir}/colord/SwappedRedAndGreen.icc
%{_icccolordir}/colord/WideGamutRGB.icc

%files help
%doc README.md NEWS
%dir %{_datadir}/gtk-doc/html/colord
%{_datadir}/gtk-doc/html/colord/*
%{_datadir}/man/man1/*.1.gz

%changelog
* Wed Mar 06 2024 liweigang <izmirvii@gmail.com> - 1.4.7-1
- update to version 1.4.7

* Thu Feb 16 2023 lilong <lilong@kylinos.cn> - 1.4.6-1
- Upgrade to  1.4.6

* Wed Aug 31 2022 wangkerong <wangkerong@h-partners.com> - 1.4.5-4
- fix CVE-2021-42523

* Mon Aug 29 2022 wangkerong <wangkerong@h-partners.com> - 1.4.5-3
- fix CVE-2021-42523

* Mon Mar 28 2022 wangkerong <wangkerong@h-partners.com> - 1.4.5-2
- enable test case

* Sat Jan 30 2021 yanglu <yanglu60@huawei.com> - 1.4.5-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to version 1.4.5

* Sat Jul 25 2020 zhangqiumiao <zhangqiumiao1@huawei.com> - 1.4.4-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to version 1.4.4

* Fri Mar 13 2020 songnannan <songnannan2@huawei.com> - 1.4.3-6
- move the files to main package

* Mon Feb 17 2020 hexiujun <hexiujun1@huawei.com> - 1.4.3-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:unpack libs subpackage

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.4.3-4
- change the path of files

* Wed Sep 18 2019 Yiru Wang <wangyiru1@huawei.com> - 1.4.3-3
- Pakcage init
